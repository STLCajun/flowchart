<?php // Test Page 

	$json = file_get_contents("flow.json");
	$data = json_decode($json, true);

?>
<!DOCTYPE html>
<html>
	<head>
	</head>

		<title>Flow Test</title>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
		<link rel="stylesheet" href="main.css">

	<body>

	<div class="flowchart">

		<div class="work_area">
			<div id="history_bucket"></div>
			<div class="nodes">
			<?php

				foreach ($data['nodes'] as $node) {

					?>

						<div class='node hidden' id='node_<?php echo $node['id'] ?>'>

							<div class="content">
								<img src="http://lorempixel.com/400/400/cats/<?php echo $node['id'] ?>" />
								<h1><?php echo $node['title']; ?></h1>
								<p><?php echo $node['text']; ?></p>

								<?php if ($node['type'] == 'question') { ?>

									<div class="responses">
									<?php foreach ($node['responses'] as $response) { ?>
										<div class="node_button" id="button_<?php echo $node['id'] ?>_<?php echo $response['id'] ?>" data-next="<?php echo $response['next-node']; ?>">
											<?php echo $response['answer']; ?>
										</div>
									<?php } ?>
									</div>

								<?php } ?>
							</div>
							<div class="history" id='history_<?php echo $node['id'] ?>' data-history="<?php echo $node['id'] ?>" >
								<img src="http://lorempixel.com/400/400/cats/<?php echo $node['id'] ?>" />
							</div>
						</div>

					<?php
				}

			?>
			</div>
		</div>
	</div>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

		<script src="main.js"></script>
	
	</body>
</html>

