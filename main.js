main = {

	init: function() {

		this.bind();
		this.start();

	},

	bind: function() {
		var context = this;

		$(document).on('click', '.node_button', function(e) {
			e.preventDefault();
			element = $(this);

			context.nextNode(element);
		});

		$(document).on('click', '#history_bucket .history', function(e) {
			e.preventDefault();
			element = $(this);

			context.showHistory(element);
		});

	},

	start: function() {

		//$('#node_1').css('display', 'block');
		$primary = $('#node_1');
		this.grow($primary);
		$primary.nextAll().each(function() {
			$(this).addClass('shrink');
		});


	},

	nextNode: function(ele) {

		$parent = ele.parents('.node');
		$parent.children('.history').clone().appendTo("#history_bucket");
		this.shrink($parent, false);
		//$parent.hide();

		var next = ele.data("next");
		var next_node = '#node_' + next;
		this.grow($('#node_' + next));

	},

	showHistory: function($ele) {
		var context = this;
		var $id = $ele.data('history');
		var $node = $('#node_'+$id);

		this.clearHistory($ele);

		$('.node').each(function() {
			context.shrink($(this));
		});

		this.grow($node);

	},

	clearHistory: function($ele) {

		this.shrink($ele, true);
		this.shrink($ele.nextAll(), true);

	},

	shrink: function($ele, $remove) {

		$ele.addClass('shrink');
		if ($remove) {

			setTimeout(function() {
				$ele.remove();
			}, 500);

		} else {
			setTimeout(function() {
				$ele.addClass('hidden');
			}, 200);
		}

	},

	grow: function($ele) {
		setTimeout(function() {
			$ele.removeClass('hidden');
			$ele.removeClass('shrink');
		}, 500);
	}

}

$(function() {
	main.init();
});